# 
# Copyright (c) 2021-2025 Ad5001
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE. 
# 
FROM ubuntu:noble

ARG DEBIAN_FRONTEND=noninteractive
ARG PYSIDE_VERSION

ENV TZ=Europe/Paris
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get -y install make git rpm dh-python \
                       xvfb xdotool wmctrl libxcb-cursor0 \
                       python3 python3-pip python3-stdeb python3-requests python3-packaging python3-pip python3-pytest python3-pytest-cov python3-pytestqt
RUN rm /usr/lib/python3.*/EXTERNALLY-MANAGED # Disable managed environment. We don't care about breaking system packages here.
RUN python3 -m pip install -U setuptools # Upgrading packages that need fixes.
RUN python3 -m pip install PySide6==$PYSIDE_VERSION
RUN apt clean
