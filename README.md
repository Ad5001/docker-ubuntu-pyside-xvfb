# docker-ubuntu-pyside-xvfb
---

This repository contains the Dockerfiles required to build all tags on [Docker Hub](https://hub.docker.com/repository/docker/ad5001/ubuntu-pyside-xvfb/):    

## All tags

- a version of Python (v3.13 for PySide6, v3.10 for PySide2)
- the tagged version of PySide2/6.
- Python packages: `pip`, `requests`, `packaging`, `pytest` (+ `pytest-cov` + `pytest-qt`)
- `xvfb`, `xdotool`, and `wmctrl`.

## Linux tags
- a Ubuntu base (oracular for PySide6 and jammy for PySide2)    
- python3 is available under `/usr/bin/python3`.    
- External pip management disabled (allows you to install packages from pip directly).     
- The corresponding pyside2/6-tools.    
- `make`, `git`, `rpm`, and `dh-python`.

Additionally, the linux-6*-latex-npde tag include a full TeXlive installation including `dvipng`, the latex extras, and `node`, `npm` installed, and qt6.    


## Wine tags
- The latest `winehq-staging` under a Ubuntu base.
- `/usr/bin/python` calling `python3` under wine.
- `/usr/bin/pip` calling `pip` under wine.
- `/usr/bin/pyinstaller` calling `pyinstaller` under wine.
- `wget`, `curl`, `git`, `p7zip-full`, `unzip` and `nsis`.

---

## Building docker files

It's recommended to use the `build.sh` bash script to build the Docker image. They use the `config.toml` in each directory to provide the data for each build,
including the specific PySide versions to use for each.

Usage:
`bash build.sh [--pyside2] [--pyside6] [--latest] [--no-push] [--repo=ad5001/ubuntu-pyside-xvfb]`

- --pyside2: Build PySide2 images.
- --pyside6: Build PySide6 images.
- --latest: (Optional) Tag these as latest.
- --no-push: (Optional) Do not push the tags to the Docker registry.
- --repo: (Optional) Repository on the Docker registry which to push the images to.

NOTE: In Docker, use xvfb-run to run a program within a Xvfb server.
