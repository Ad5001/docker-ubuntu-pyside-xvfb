#!/bin/bash
# 
# Copyright (c) 2021-2024 Ad5001
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE. 
# 

# shellcheck disable=SC2046

# Resets all ansi codes.
RESET="\e[0m"
# Sets the forground color to light green.
L_GREEN_FG="\e[32m"
# Sets the forground color to red.
RED_FG="\e[38;5;204m"

pyside6=false
pyside2=false
latest=false
push=true
repo="ad5001/ubuntu-pyside-xvfb"

# Display text in a box
# Signature: (<string message>) -> string
box() {
    len=${#1}
    echo "┌─$(yes ─ | head -$len | tr -d "\n")─┐"
    echo "│ $1 │"
    echo "└─$(yes ─ | head -$len | tr -d "\n")─┘"
}

box-red() {
    echo -n -e "$RED_FG"
    box "$@"
    echo -n -e "$RESET"
}

box-green() {
    echo -n -e "$L_GREEN_FG"
    box "$@"
    echo -n -e "$RESET"
}

# Returns a string of KEY="VALUES" separated by newlines for the corresponding category.
# Signature: (<string source_file>, <string dict_name>) -> string
from-toml-dict() {
    if [ -z "$2" ]; then
        sed -n -e '1,/\[/ p' "$1" | grep '="'
    else
        sed -n -e '/\['"$2"']/,/\[/ p' "$1" | grep '="'
    fi
}

# Replaces {KEY} by value in given context.
# Signature: stdin+(<string key>, <string value>) -> string
pass-argument() {
    while read -r line; do
        echo "${line//\{$1\}/$2}"
    done
}

# Queries a value from an INI/singular dict TOML into for a given key
# Signature: stdin+(<string key>) -> string
get-value-of() {
    while read -r line; do
        if [ "${line%=*}" = "$1" ]; then
            echo "$line" | cut -d'"' -f 2
            return 0
        fi
    done
}

# Converts arguments from an INI/singular dict TOML file into a list of build args for docker.
# Signature: stdin+(<string repo>, <string pyside_version>) -> string
to-docker-args() {
    # Read stdin
    out=""
    while read -r line; do
        key="${line%=*}"
        value="$(echo "${line}" | get-value-of "$key" | pass-argument REPO "$1" | pass-argument VERSION "$2")"
        out="$out--build-arg $key=$value ";
    done
    echo "$out"
}

# Builds a pyside directory (with a config.toml for additional parameters.
# Signature (<string directory>) -> void
build_pyside() {
    directory=$1
    cfg="$directory/config.toml"
    # Query information from config
    major=$(from-toml-dict "$cfg" | get-value-of MAJOR_VERSION)
    version=$(from-toml-dict "$cfg" | get-value-of PYSIDE_VERSION)
    for build in $(from-toml-dict "$cfg" | get-value-of BUILDS); do
        # Build each build individidually
        tag="$(from-toml-dict "$cfg" "$build.settings" | get-value-of TAG | pass-argument VERSION "$version")"
        latest_tag="$(from-toml-dict "$cfg" "$build.settings" | get-value-of TAG | pass-argument VERSION "${major}-latest")"
        build_args="$(from-toml-dict "$cfg" "$build.docker-args" | to-docker-args "$repo" "$version")"
        box-green "Building image $repo:$tag..."
        echo "> docker build -t $repo:$tag $build_args --build-arg PYSIDE_VERSION=$version $directory/${build}"
        docker build -t "$repo:$tag" $build_args --build-arg PYSIDE_VERSION="$version" "$directory/${build}"
        # Tag as latest
        if [ "$latest" != "false" ]; then
            docker tag "$repo:$tag" "$repo:$latest_tag"
        fi
        # Push builds
        if [ "$push" != "false" ]; then
            if [ "$latest" != "false" ]; then
                box-green "Pushing image $repo:$tag and $repo:$latest_tag..."
                docker push "$repo:$latest_tag"
                docker push "$repo:$tag"
            else
                box-green "Pushing image $repo:$tag..."
                docker push "$repo:$tag"
            fi
        fi
    done
}

# Query arguments

while [ $# -gt 0 ]; do
    case "$1" in
        --pyside6)
            pyside6=true
            ;;
        --pyside2)
            pyside2=true
            ;;
        --latest)
            latest=true
            ;;
        --no-push)
            push=false
            ;;
        --repo=*)
            repo="${1#*=}"
            ;;
        *)
            box "Error: Invalid argument."
            exit 1
    esac
    shift
done

if [ "$pyside6" = "false" ] && [ "$pyside2" = "false" ]; then
    box-red "Must at least either use --pyside2 or --pyside6"
fi

# Build PySide6
if [ "$pyside6" != "false" ]; then
    build_pyside "pyside6"
fi

# Build PySide2
if [ "$pyside2" != "false" ]; then
    build_pyside "pyside2"
fi
